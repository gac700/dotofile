if &compatible
  set nocompatible               " Be iMproved
endif

set number
set encoding=utf-8

set tabstop=2
set shiftwidth=2
set expandtab


colorscheme jellybeans

" Required:
set runtimepath+=/Users/gac700/.cache/dein/repos/github.com/Shougo/dein.vim
let g:python3_host_prog = expand('/Users/gac700/.pyenv/shims/python3')

" Required:
if dein#load_state('/Users/gac700/.cache/dein')
  call dein#begin('/Users/gac700/.cache/dein')

  " Let dein manage dein
  " Required:
  call dein#add('/Users/gac700/.cache/dein/repos/github.com/Shougo/dein.vim')

  " Add or remove your plugins here like this:
  " :call dein#update()
  call dein#add('Shougo/neosnippet.vim')
  call dein#add('Shougo/neosnippet-snippets')
  call dein#add('Shougo/denite.nvim')
  call dein#add('iyuuya/denite-rails')
  call dein#add('preservim/nerdtree')
  call dein#add('Shougo/neoyank.vim')

  " Required:
  call dein#end()
  call dein#save_state()
endif

" Required:
filetype plugin indent on
syntax enable

" If you want to install not installed plugins on startup.
"if dein#check_install()
"  call dein#install()
"endif
"
"Denite
autocmd FileType denite call s:denite_my_settings()
function! s:denite_my_settings() abort
  nnoremap <silent><buffer><expr> <CR>
  \ denite#do_map('do_action')
  nnoremap <silent><buffer><expr> d
  \ denite#do_map('do_action', 'delete')
  nnoremap <silent><buffer><expr> p
  \ denite#do_map('do_action', 'preview')
  nnoremap <silent><buffer><expr> q
  \ denite#do_map('quit')
  nnoremap <silent><buffer><expr> i
  \ denite#do_map('open_filter_buffer')
  nnoremap <silent><buffer><expr> <Space>
  \ denite#do_map('toggle_select').'j'
endfunction
" Denite Use ag
call denite#custom#var('file_rec', 'command', ['ag', '--follow', '--nocolor', '--nogroup', '-g', ''])
call denite#custom#var('grep', 'command', ['ag'])
call denite#custom#var('grep', 'recursive_opts', [])
call denite#custom#var('grep', 'pattern_opt', [])
call denite#custom#var('grep', 'default_opts', ['--follow', '--no-group', '--no-color'])
call denite#custom#source('file_rec', 'matchers', ['matcher_fuzzy','matcher_ignore_globs'])
call denite#custom#filter('matcher_ignore_globs', 'ignore_globs',
      \ [
      \ '.git/', 'build/', '__pycache__/',
      \ 'images/', '*.o', '*.make',
      \ '*.min.*','node_modules/', '.docker-sync/',
      \ 'img/', 'fonts/'])
" Define mappings
hi CursorLine guifg=#E19972
" ノーマルモードで起動
call denite#custom#option('default', {'mode': 'normal'})
" jjでノーマルモードに
call denite#custom#map('insert', 'jj', '<denite:enter_mode:normal>', 'noremap')
" grep検索
nnoremap <silent> ,g  :<C-u>Denite grep -buffer-name=search-buffer-denite<CR>
" カーソル位置の単語をgrep検索
nnoremap <silent> ,cg :<C-u>DeniteCursorWord grep -buffer-name=search-buffer-denite<CR>
" grep検索結果の再呼出
nnoremap <silent> ,gg  :<C-u>Denite -resume  -buffer-name=search-buffer-denite<CR>

nnoremap <silent> ,ub :<C-u>Denite buffer<CR>

"Rails
noremap ,rc :<C-u>Denite rails:controller<CR>
noremap ,rm :<C-u>Denite rails:model<CR>
noremap ,rv :<C-u>Denite rails:view<CR>
noremap ,rs :<C-u>Denite rails:service<CR>




let g:NERDTreeDirArrows = 1
let g:NERDTreeDirArrowExpandable  = '▶'
let g:NERDTreeDirArrowCollapsible = '▼'
let g:NERDTreeNodeDelimiter = "\u00a0"
nnoremap <silent><C-e> :NERDTreeToggle<CR>



nnoremap <silent> ,uy :<C-u>Denite neoyank<CR>




" other key map
nnoremap ss :<C-u>sp<CR>
nnoremap sv :<C-u>vs<CR>
nnoremap sn <C-w>w
nnoremap sj <C-w>j
nnoremap sk <C-w>k
nnoremap sl <C-w>l
nnoremap sh <C-w>h
