ln -sf ~/dotofile/.vimrc ~/.vimrc
ln -sf ~/dotofile/.vim ~/.vim
ln -sf ~/dotofile/.bash_profile ~/.bash_profile
ln -sf ~/dotofile/.gitconfig ~/.gitconfig
ln -sf ~/dotofile/init.vim ~/.config/nvim

